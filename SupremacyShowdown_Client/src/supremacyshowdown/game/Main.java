package supremacyshowdown.game;

import java.util.ArrayList;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class Main extends Application {
	
	public enum gameState{
		PREGAME, INGAME, MENU, PAUSE, POSTGAME, POSTGAMETOMENU, MENUTOPREGAME,SPLASH
	}
	
	private static Stage stage;
	private static Canvas cCanvas;
	private static Canvas rCanvas;
	private static Canvas lCanvas;
	private static Canvas clockCanvas;
	private static ArrayList<KeyCode> pressedKeys;
	
	private static gameState state;
	private static GraphicsContext clock;
	private static boolean tickOne;
	private static int countdown;
	private static int splash = 0;

	private Button startButton;

	// Dropdowns for Player/AI choice
    private ComboBox<String> comboBoxP1P, comboBoxP2P, comboBoxP3P, comboBoxP4P;

	// Dropdowns for Civilization choice
    private ComboBox<String> comboBoxP1C, comboBoxP2C, comboBoxP3C, comboBoxP4C;
	
	// Lists to store data selected
    private boolean[] playerIsHuman = new boolean[4];
    private int[] playerCivilization = new int[4];
	
	private static gameState returnState;
	
	/**
	 * Starts the Game. Opens the human/AI and Civ selection menu.
	 * Also controls gamestates.
	 */
	@Override
	public void start(Stage primaryStage) {

		state = gameState.SPLASH;	// set initial gamestate
		tickOne = true;			
		
		stage = primaryStage;
		stage.getIcons().add(new Image("supremacyshowdown/game/images/RomanDefault.png"));	// set game icon - ONLY SHOWS UP IN WINDOWS, UBUNTU HAS A UI ERROR THAT JAVA HAVE NOT FIXED
		stage.setTitle("Supremacy Showdown!");	// hell yea what a game title, top notch
		
		BorderPane gameScreen = new BorderPane();	// setup the panes
		BorderPane menuScreen = new BorderPane();
		BorderPane splashScreen = new BorderPane();
		
		pressedKeys = new ArrayList<KeyCode>();// initialise the pressed keys arraylist
		
		gameScreen.setPrefSize(1024, 768);// set game screen sizes
		menuScreen.setPrefSize(500, 500);
		splashScreen.setPrefSize(500, 500);

		
		// Create the canvas for the opening splash screen
		Canvas splashCanvas=new Canvas(500,500);
		Image splashImage = new Image("supremacyshowdown/game/images/SplashBackground.png");
		splashCanvas.getGraphicsContext2D().drawImage(splashImage, 0, 0,500,500);
		splashScreen.getChildren().add(splashCanvas);
		
		VisualController vc = new VisualController();	// create visual controller for game control
		
		rCanvas = vc.getGameRightCanvas();				// and the game canvasses
		gameScreen.getChildren().add(rCanvas);
		
		cCanvas = vc.getGameCenterCanvas();
		gameScreen.getChildren().add(cCanvas);
		
		lCanvas = vc.getGameLeftCanvas();
		gameScreen.getChildren().add(lCanvas);
		
		
		
		
		Scene gameScene = new Scene(gameScreen);		// create the game screen scene
		Scene splashScene = new Scene(splashScreen);
		
		
		// "Start" button
		startButton = new Button("START GAME");
		startButton.setStyle("-fx-background-color: #ff9999;");
		startButton.setMinSize(150, 50);
		startButton.setFont(Font.font("Tahoma", FontWeight.BOLD, 16));
		
		startButton.setOnAction(event -> {
		    this.storeValues();					// stores the game creation values in le database
		    state = gameState.MENUTOPREGAME;	// set game state to shift from the menu to the Pregame
		    
		});
		
		// Dropdown for Human/AI select
		comboBoxP1P = new ComboBox<>();
		comboBoxP2P = new ComboBox<>();
		comboBoxP3P = new ComboBox<>();
		comboBoxP4P = new ComboBox<>();
		
		// Dropdown for Civilization select
		comboBoxP1C = new ComboBox<>();
		comboBoxP2C = new ComboBox<>();
		comboBoxP3C = new ComboBox<>();
		comboBoxP4C = new ComboBox<>();
		
		// Add 2 options to dropdown menu (Human or AI)
		comboBoxP1P.getItems().addAll("Human", "AI");
		comboBoxP2P.getItems().addAll("Human", "AI");
		comboBoxP3P.getItems().addAll("Human", "AI");
		comboBoxP4P.getItems().addAll("Human", "AI");
		
		// Default Values
		comboBoxP1P.setValue("Human");
		comboBoxP2P.setValue("Human");
		comboBoxP3P.setValue("Human");
		comboBoxP4P.setValue("Human");
		
		// Add 4 options to dropdown menu (Civilization)
		comboBoxP1C.getItems().addAll("Amazon", "Roman", "Samurai", "Viking");
		comboBoxP2C.getItems().addAll("Amazon", "Roman", "Samurai", "Viking");
		comboBoxP3C.getItems().addAll("Amazon", "Roman", "Samurai", "Viking");
		comboBoxP4C.getItems().addAll("Amazon", "Roman", "Samurai", "Viking");
		
		// Default Values
		comboBoxP1C.setValue("Amazon");
		comboBoxP2C.setValue("Roman");
		comboBoxP3C.setValue("Samurai");
		comboBoxP4C.setValue("Viking");
		
		// Horizontal layouts, spacing of 10
		HBox layoutP1 = new HBox(10);
		layoutP1.setAlignment(Pos.CENTER);
		layoutP1.setPadding(new Insets(20, 20, 20, 20));
		layoutP1.getChildren().addAll(comboBoxP1P, comboBoxP1C);
		
		HBox layoutP2 = new HBox(10);	
		layoutP2.setAlignment(Pos.CENTER);
		layoutP2.setPadding(new Insets(20, 20, 20, 20));
		layoutP2.getChildren().addAll(comboBoxP2P, comboBoxP2C);
		
		HBox layoutP3 = new HBox(10);
		layoutP3.setAlignment(Pos.CENTER);
		layoutP3.setPadding(new Insets(20, 20, 20, 20));
		layoutP3.getChildren().addAll(comboBoxP3P, comboBoxP3C);
		
		HBox layoutP4 = new HBox(10);	
		layoutP4.setAlignment(Pos.CENTER);
		layoutP4.setPadding(new Insets(20, 20, 20, 20));
		layoutP4.getChildren().addAll(comboBoxP4P, comboBoxP4C);
		
		HBox layoutStart = new HBox(10);
		layoutStart.setAlignment(Pos.CENTER);
		layoutStart.setPadding(new Insets(20, 20, 20, 20));
		layoutStart.getChildren().addAll(startButton);

		VBox layout = new VBox(4);
		layout.setAlignment(Pos.CENTER);
		layout.setPrefSize(500, 500);
		layout.setPadding(new Insets(92, 0, 20, 115));
		layout.getChildren().addAll(layoutP1, layoutP2, layoutP3,
				layoutP4, layoutStart);
		
		BackgroundImage BI = new BackgroundImage(new Image("supremacyshowdown/game/images/BackgroundMenu.png",500,500,true,true),
		        BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
		          BackgroundSize.DEFAULT);
		layout.setBackground(new Background(BI));
		
		Scene menuScene = new Scene(layout);	// create the menu scene
		
		
		
		new AnimationTimer() {				// here is where our game screen refreshes, doing things based on current state
			
			@Override
			public void handle(long now) {
				// in menu mode, just idle until the start button is pressed
				switch (state) {
					case SPLASH:
						stage.setScene(splashScene);
						splash++;
						if (splash > 180) {
							state = gameState.MENU;
						}
						break;

					case MENU:
						stage.setScene(menuScene);
						break;

					case PREGAME:    // in pregame, the countdown is initiated, then the game is entered
						if (tickOne) {
							countdown = 180;
							vc.newGame(playerIsHuman, playerCivilization);
							vc.draw();

							clockCanvas = new Canvas(1024, 768);
							clock = clockCanvas.getGraphicsContext2D();
							clock.save();
							clock.setFont(new Font((double) 50));
							gameScreen.getChildren().add(clockCanvas);
							tickOne = false;

							// need to try connect to server again and wait for go signal

						}
						clock.clearRect(0, 0, 1024, 768);
						clock.setFill(Color.BLACK);
						clock.fillText(Integer.toString((int) ((float) countdown / (float) 60)), 1024 / 2, 50);

						if (countdown == 0) {
							state = gameState.INGAME;

						}

						countdown--;
						break;

					case INGAME:
						if (!vc.getIfFinished()) {

							if (pressedKeys.contains(KeyCode.P)) {
								returnState = gameState.INGAME;
								state = gameState.PAUSE;
							}

							vc.tickGame(pressedKeys);
							vc.draw();

							gameScreen.getChildren().remove(clockCanvas);
							gameScreen.getChildren().add(clockCanvas);

							clock.clearRect(0, 0, 1024, 768);
							clock.setFill(Color.BLACK);
							clock.fillText(Integer.toString((int) ((float) vc.getTimeRemaining() / (float) 60)), 460, 50);

						} else {
							state = gameState.POSTGAME;
						}
						break;

					case PAUSE:

						if (pressedKeys.contains(KeyCode.ESCAPE)) {
							state = gameState.POSTGAMETOMENU;
						} else if (pressedKeys.contains(KeyCode.P)) {
							clock.clearRect(0, 0, 1024, 768);
							clock.setFill(Color.BLACK);
							clock.fillText("PAUSE\nPress esc to exit", 300, 375);
						} else {
							state = returnState;
						}
						break;

					case MENUTOPREGAME:
						stage.setScene(gameScene);
						state = gameState.PREGAME;
						tickOne = true;
						break;

					case POSTGAMETOMENU:
						stage.setScene(menuScene);
						state = gameState.MENU;
						pressedKeys.remove(KeyCode.P);
						clock.clearRect(0, 0, 1024, 768);
						break;

					case POSTGAME:
						int winner = vc.getWinner();
						clock.clearRect(0, 0, 1024, 768);
						clock.setFill(Color.BLACK);
						clock.fillText(("Player " + Integer.toString(winner) + " wins!\nPress esc to return to menu"), 250, 375);

						if (pressedKeys.contains(KeyCode.ESCAPE)) {
							state = gameState.POSTGAMETOMENU;
						}
						break;
				}
				
			}
		}.start();
		
		gameScene.setOnKeyPressed(e ->{			// gets all the keys that are pressed on the keyboard and stores them in an arraylist
		    if(e.getCode() == KeyCode.RIGHT){
		        pressedKeys.add(KeyCode.RIGHT);		    
		    }
		    if(e.getCode() == KeyCode.LEFT){
		        pressedKeys.add(KeyCode.LEFT);		    
		    }
		    if(e.getCode() == KeyCode.A){
		        pressedKeys.add(KeyCode.A);		    
		    }
		    if(e.getCode() == KeyCode.D){
		        pressedKeys.add(KeyCode.D);		    
		    }
		    if(e.getCode() == KeyCode.J){
		        pressedKeys.add(KeyCode.J);		    
		    }
		    if(e.getCode() == KeyCode.L){
		        pressedKeys.add(KeyCode.L);
		    }
		    if(e.getCode() == KeyCode.NUMPAD4){
		        pressedKeys.add(KeyCode.NUMPAD4);		    
		    }
		    if(e.getCode() == KeyCode.NUMPAD6){
		        pressedKeys.add(KeyCode.NUMPAD6);
		    }
		    if(e.getCode() == KeyCode.P){			// pause is a special case, which is either added or removed when the key is pressed
		    	if(pressedKeys.contains(KeyCode.P)){
		    		pressedKeys.remove(KeyCode.P);
		    	}
		    	else {
		    		pressedKeys.add(KeyCode.P);
		    	}
		    }
		    if(e.getCode() == KeyCode.PAGE_DOWN){	// pagedown is also a special case, setting the time remaining in the game to 1 tick,
		    	vc.setTimeRemaining(1);				// effectively ending the game instantly
		    }
		    if(e.getCode() == KeyCode.ESCAPE){
		    	pressedKeys.add(KeyCode.ESCAPE);
		    }
		    
		});
		
		gameScene.setOnKeyReleased(e ->{				// removes any keys from the array when they're released
		    if(e.getCode() == KeyCode.RIGHT){
		        while(pressedKeys.remove(KeyCode.RIGHT));
		    }
		    if(e.getCode() == KeyCode.LEFT){
		    	while(pressedKeys.remove(KeyCode.LEFT));
		    }
		    if(e.getCode() == KeyCode.A){
		    	while(pressedKeys.remove(KeyCode.A));
		    }
		    if(e.getCode() == KeyCode.D){
		    	while(pressedKeys.remove(KeyCode.D));
		    }
		    if(e.getCode() == KeyCode.J){
		    	while(pressedKeys.remove(KeyCode.J));
		    }
		    if(e.getCode() == KeyCode.L){
		    	while(pressedKeys.remove(KeyCode.L));
		    }
		    if(e.getCode() == KeyCode.NUMPAD4){
		    	while(pressedKeys.remove(KeyCode.NUMPAD4));
		    }
		    if(e.getCode() == KeyCode.NUMPAD6){
		    	while(pressedKeys.remove(KeyCode.NUMPAD6));    
		    }
		    if(e.getCode() == KeyCode.ESCAPE){
		    	while(pressedKeys.remove(KeyCode.ESCAPE));
		    }
		    
		});
		
		menuScene.setOnKeyReleased(e ->{						// in the menu scene, allow the escape key to be removed from the array
			if(e.getCode() == KeyCode.ESCAPE){					// This happens when someone exits to the menu from a game - they press
		    	while(pressedKeys.remove(KeyCode.ESCAPE));		// the escape key but it never has a chance to leave the array unless
		    													// this code is included.
		    }
		    
		});
		
		stage.show();	// and show the stage
	}
	/**
	 * Stores the AI/Human and civ values currently selected in the menu for game creation
	 */
    private void storeValues()	{
		
		switch(comboBoxP1P.getValue())	{
		case "Human":
			playerIsHuman[0] = true;
			break;
		case "AI":
			playerIsHuman[0] = false;
			break;
		}
		
		switch(comboBoxP2P.getValue())	{
		case "Human":
			playerIsHuman[1] = true;
			break;
		case "AI":
			playerIsHuman[1] = false;
			break;
		}
		
		switch(comboBoxP3P.getValue())	{
		case "Human":
			playerIsHuman[2] = true;
			break;
		case "AI":
			playerIsHuman[2] = false;
			break;
		}
		
		switch(comboBoxP4P.getValue())	{
		case "Human":
			playerIsHuman[3] = true;
			break;
		case "AI":
			playerIsHuman[3] = false;
			break;
		}
		
		switch(comboBoxP1C.getValue())	{
		case "Amazon":
			playerCivilization[0] = 0;
			break;
		case "Roman":
			playerCivilization[0] = 1;
			break;
		case "Samurai":
			playerCivilization[0] = 2;
			break;
		case "Viking":
			playerCivilization[0] = 3;
			break;
		}
		
		switch(comboBoxP2C.getValue())	{
		case "Amazon":
			playerCivilization[1] = 0;
			break;
		case "Roman":
			playerCivilization[1] = 1;
			break;
		case "Samurai":
			playerCivilization[1] = 2;
			break;
		case "Viking":
			playerCivilization[1] = 3;
			break;
		}
		
		switch(comboBoxP3C.getValue())	{
		case "Amazon":
			playerCivilization[2] = 0;
			break;
		case "Roman":
			playerCivilization[2] = 1;
			break;
		case "Samurai":
			playerCivilization[2] = 2;
			break;
		case "Viking":
			playerCivilization[2] = 3;
			break;
		}
		
		switch(comboBoxP4C.getValue())	{
		case "Amazon":
			playerCivilization[3] = 0;
			break;
		case "Roman":
			playerCivilization[3] = 1;
			break;
		case "Samurai":
			playerCivilization[3] = 2;
			break;
		case "Viking":
			playerCivilization[3] = 3;
			break;
		}
		
	}

	/**
	 * Main method of the game. Launches the game.
	 * @param args should be null
	 */
	public static void main(String[] args) {
		launch(args);
	}
	
}
