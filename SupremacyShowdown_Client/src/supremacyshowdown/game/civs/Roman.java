package supremacyshowdown.game.civs;

import javafx.scene.image.Image;
import supremacyshowdown.game.interfaces.CivInterface;

/**
	* This object represents the "Roman" civilization.
	* 
	*  @author M GOODALL
	*  @version 08/04/2017
*/

public class Roman implements CivInterface {
	
	private int civ;
	private int paddleWidth;
	private int paddleSpeed;
	private Image paddleFillV;
	private Image paddleFillH;
	private Image brickV;
	private Image brickH;
	private Image spriteDefault;
	private Image spriteSurprised;
	private Image spritePortrait;
	
	/*
	 * Default values in the constructor
	 * 
	 */
	public Roman() {
		civ = 1;
		paddleWidth = 80;	// Widest paddle
		paddleSpeed = 6;	// Slowest speed
		paddleFillV = new Image("supremacyshowdown/game/images/RomanPaddleV.png");
		paddleFillH = new Image("supremacyshowdown/game/images/RomanPaddleH.png");
		brickV = new Image("supremacyshowdown/game/images/RomanBrickV.png");
		brickH = new Image("supremacyshowdown/game/images/RomanBrickH.png");
		spriteDefault = new Image("supremacyshowdown/game/images/RomanDefault.png");
		spriteSurprised = new Image("supremacyshowdown/game/images/RomanSurprised.png");
		spritePortrait = new Image("supremacyshowdown/game/images/RomanPortrait.png");

	}
	
	/*
	 * Setter methods
	 */
	@Override
	public void setCiv(int c)	{
		civ = c;
	}
	
	@Override
	public void setPaddleWidth(int pw)	{
		paddleWidth = pw;
	}
	
	@Override
	public void setPaddleSpeed(int ps)	{
		paddleSpeed = ps;
	}
	

	
	/*
	 * Getter methods
	 */
	@Override
	public int getCivilization()	{
		return civ;
	}
	
	@Override
	public int getPaddleWidth()	{
		return paddleWidth;
	}
	
	@Override
	public int getPaddleSpeed()	{
		return paddleSpeed;
	}

	@Override
	public Image getPaddleFillV() {	return paddleFillV;	}

	@Override
	public Image getPaddleFillH() {
		return paddleFillH;
	}

	@Override
	public Image getBrickV() {
		return brickV;
	}

	@Override
	public Image getBrickH() {
		return brickH;
	}

	@Override
	public Image getSpriteDefault() {
		return spriteDefault;
	}

	@Override
	public Image getSpriteSurprised() {
		return spriteSurprised;
	}

	@Override
	public Image getSpritePortrait() {
		return spritePortrait;
	}
	
}

	