package supremacyshowdown.game;

import java.util.ArrayList;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import supremacyshowdown.game.civs.Amazon;
import supremacyshowdown.game.civs.Roman;
import supremacyshowdown.game.civs.Samurai;
import supremacyshowdown.game.civs.Viking;
import supremacyshowdown.game.interfaces.CivInterface;
import supremacyshowdown.game.models.*;
import supremacyshowdown.game.models.Paddle.orientation;

public class VisualController {
	
	private ArrayList<Ball> balls;
	private ArrayList<Wall> walls;
	private ArrayList<Warlord> warlords;
	private ArrayList<Paddle> paddles;
	
	private Game currentGame;
	
	private Image backgroundtest = new Image("supremacyshowdown/game/images/BackgroundForest.png");

	private Canvas gameLeft;
	private Canvas gameRight;
	private Canvas gameCenter;
	
	private static Amazon amazon;
	private static Viking viking;
	private static Samurai samurai;
	private static Roman roman;
	
	// coordinates, widths, and heights for all walls in the game
	private static int[] wallTopLeftX = {0,88,26,126,0,88,176,151,229,190,151,190,229,229};
	private static int[] wallTopLeftY = {151,151,190,190,229,229,229,0,0,25,88,126,88,176};
	private static int[] wallTopLeftW = {63,63,76,63,63,63,50,25,25,25,25,25,25,25};
	private static int[] wallTopLeftH = {25,25,25,25,25,25,25,63,63,76,63,63,63,50};
		
	private static int[] wallTopRightX = {513,591,552,513,552,591,513,616,704,577,665,538,616,704};
	private static int[] wallTopRightY = {0,0,25,88,126,88,176,151,151,190,190,229,229,229};
	private static int[] wallTopRightW = {25,25,25,25,25,25,25,63,63,63,76,50,63,63};
	private static int[] wallTopRightH = {63,63,76,63,63,63,50,25,25,25,25,25,25,25};
	
	private static int[] wallBottomRightX = {538,616,704,577,665,616,704,513,513,513,552,552,591,591};
	private static int[] wallBottomRightY = {513,513,513,552,552,591,591,538,616,704,577,665,616,704};
	private static int[] wallBottomRightW = {50,63,63,63,76,63,63,25,25,25,25,25,25,25};
	private static int[] wallBottomRightH = {25,25,25,25,25,25,25,50,63,63,63,76,63,63};
	
	private static int[] wallBottomLeftX = {0,88,176,26,126,0,88,229,229,229,190,190,151,151};
	private static int[] wallBottomLeftY = {513,513,513,552,552,591,591,538,616,704,577,665,616,704};
	private static int[] wallBottomLeftW = {63,63,50,76,63,63,63,25,25,25,25,25,25,25};
	private static int[] wallBottomLeftH = {25,25,25,25,25,25,25,50,63,63,63,76,63,63,63};
	
	private Image brickTest;
	private Image warlordtest;
	
	/**
	 * constructs a visual controller object. 
	 * Creates 3 canvasses for the various parts of the game screen.
	 * Also creates an instance of each type of civilisation class.
	 */
	
	public VisualController(){
		
		gameCenter = new Canvas(896, 768); // add 128 to all X coordinates when drawing things in game center
		gameLeft = new Canvas(128, 768);
		gameRight = new Canvas(1024, 768); // make sure to only draw in the last 128 columns
		
		samurai = new Samurai();// create instances of civs, to get information from them
		viking = new Viking();
		amazon = new Amazon();
		roman = new Roman();
		
	}
	/**
	 * Gets the center part of the game canvas
	 * @return JavaFX Canvas
	 */
	public Canvas getGameCenterCanvas(){
		return gameCenter;
	}
	/**
	 * Gets the left part of the game canvas
	 * @return JavaFX Canvas
	 */
	public Canvas getGameLeftCanvas(){
		return gameLeft;
	}
	/**
	 * Gets the right part of the game canvas
	 * @return JavaFX Canvas
	 */
	public Canvas getGameRightCanvas(){
		return gameRight;
	}
	
	/**
	 * Creates a game based on the following input parameters:
	 * @param playerIsHuman - boolean array that describes whether a player is human(true) or not(false)
	 * @param playerCivilization - lists each civ selected by that player
	 */
	public void newGame(boolean[] playerIsHuman, int[] playerCivilization){
		
		CivInterface civ[] = new CivInterface[4];
		
		// assigning integers to civs
		for(int i = 0; i<4; i++){
			switch(playerCivilization[i]){
			case 0: civ[i] = amazon;break;
			case 1: civ[i] = roman;break;
			case 2: civ[i] = samurai;break;
			case 3: civ[i] = viking;break;
			}
		}
		 
		
		balls = 	createGameBalls();// create all the objects require for a game
		
		warlords = 	createGameWarlords(	civ[0], civ[1], civ[2], civ[3]);
		
		walls = 	createGameWalls(	civ[0], civ[1], civ[2], civ[3], warlords);
		
		paddles = 	createGamePaddles(	civ[0], civ[1], civ[2], civ[3]);
		
		
		currentGame = new Game(walls, balls, paddles, warlords, 7200, 768, playerIsHuman); // and create the game
		
		
		// finally, create the portrait for the characters outside the game zone.
		gameLeft.getGraphicsContext2D().drawImage(warlords.get(2).getNormalPic(), 0, 768-128, 128, 128);
		gameLeft.getGraphicsContext2D().drawImage(warlords.get(0).getNormalPic(), 0, 0, 128, 128);
		gameRight.getGraphicsContext2D().drawImage(warlords.get(1).getNormalPic(), 1024-128, 0, 128, 128);
		gameRight.getGraphicsContext2D().drawImage(warlords.get(3).getNormalPic(), 1024-128, 768-128, 128, 128);
	}
	/**
	 * sets the number of ticks to play in the game.
	 * @param time int number of ticks 
	 */
	public void setTimeRemaining(int time){
		currentGame.setTimeRemaining(time);
	}
	/**
	 * returns the number of ticks left to play.
	 * @return int number of ticks
	 */
	public int getTimeRemaining(){
		return currentGame.getTimeRemaining();
	}
	/**
	 * tickGame tells the game object created by the VisualController to tick once
	 * it passes through all key presses for that tick
	 * 
	 * @param throughput ArrayList of KeyCodes
	 */
	public void tickGame(ArrayList<KeyCode> throughput){
		try {
            currentGame.tick(throughput);
        }
        catch ( java.io.IOException | ClassNotFoundException e1) {
            System.out.println("Error: " + e1);
        }

	}
	/**
	 * returns whether the game is finished.
	 * @return Boolean true if the game is finished
	 */
	public boolean getIfFinished(){
		return currentGame.isFinished();
	}
	/**
	 * This method draws all objects on the game screen.
	 */
	public void draw(){
		// setup for drawing objects
		GraphicsContext gcL = gameLeft.getGraphicsContext2D();
		GraphicsContext gcR = gameRight.getGraphicsContext2D();
		
		gcL.setFill(Color.WHITE);
		gcR.setFill(Color.WHITE);
		
		gcL.fillRect(0,0,128,768);
		gcR.fillRect(0,0,1024,768);
		// draws the portraits of the players out of the game area. Above them, draws their respective brick counts.
		
		gcL.drawImage(warlords.get(0).getNormalPic(), 0, 0, 128, 128);
		gcR.drawImage(warlords.get(1).getNormalPic(), 1024-128, 0, 128, 128);
		gcL.drawImage(warlords.get(2).getNormalPic(), 0, 768-128, 128, 128);
		gcR.drawImage(warlords.get(3).getNormalPic(), 1024-128, 768-128, 128, 128);
		// draw brick count
		gcL.setFill(Color.BLACK);
		gcL.fillText("Bricks:\n" + Integer.toString(warlords.get(0).getBricks()), 20, 178);
		gcL.fillText("Bricks:\n" + Integer.toString(warlords.get(2).getBricks()), 20, 768-188);
		gcR.setFill(Color.BLACK);
		gcR.fillText("Bricks:\n" + Integer.toString(warlords.get(1).getBricks()), 1024-108, 178);
		gcR.fillText("Bricks:\n" + Integer.toString(warlords.get(3).getBricks()), 1024-108, 768-188);
		
		// now we draw the game screen
		
		GraphicsContext gc = gameCenter.getGraphicsContext2D();
		
		gc.setFill(Color.WHITE);
		gc.fillRect(0, 0, 896, 768);
		gc.drawImage(backgroundtest, 128,0,768,768);
		
		// draw eac wall, checking that it hasn't been destroyed
		
		for (int i = 0; i < walls.size(); i++){
			if (!walls.get(i).isDestroyed()){
				
				if(walls.get(i).getWidth() > walls.get(i).getHeight()){
					brickTest = walls.get(i).getHImagePath();
				}
				else {
					brickTest = walls.get(i).getVImagePath();
				}
				gc.drawImage(brickTest,walls.get(i).getXPos() + 128, walls.get(i).getYPos(), walls.get(i).getWidth(), walls.get(i).getHeight());
			}
		}
		// draw any balls (usually only 1)
		for (int i = 0; i < balls.size(); i++){
			brickTest = new Image("supremacyshowdown/game/images/ball.png");
			gc.drawImage(brickTest, balls.get(i).getXPos() + 128, balls.get(i).getYPos(), balls.get(i).getDimension(), balls.get(i).getDimension());
		}
		// draw the paddles, checking that it's owner is alive
		for (int i = 0; i < paddles.size(); i++){
			for(int y = 0; y<warlords.size(); y++){
				if (warlords.get(y).getPlayerIndex() == paddles.get(i).getOwner()){
					if(!warlords.get(y).isDead()){
						if(paddles.get(i).hOrV() == orientation.HORIZONTAL){
							
							warlordtest = paddles.get(i).getHImage();
						}
						else{
							warlordtest = paddles.get(i).getVImage();
						}
						gc.drawImage(warlordtest, paddles.get(i).getXPos() + 128, paddles.get(i).getYPos()-paddles.get(i).getHeight(), paddles.get(i).getWidth(), paddles.get(i).getHeight());
					}
					
					
				}
			}
		}
		// draw all on-screen warlords
		for (int i = 0; i < warlords.size(); i++){
			if(!warlords.get(i).isDead()){
				warlordtest = warlords.get(i).getGamePic();
				gc.drawImage(warlordtest, warlords.get(i).getXPos() + 128, warlords.get(i).getYPos(), warlords.get(i).getWidth(), warlords.get(i).getHeight());
				
			}
		}
	}
	/**
	 * gets the winner of the game
	 * @return returns the index of the winning warlord
	 */
	public int getWinner(){
		int out = 0;
		if(currentGame.isFinished()){
			for(int i = 0; i< warlords.size(); i++){
				if(warlords.get(i).hasWon()){
					out = i + 1;
				}
			}
		}
		return out;
	}
	
	
	
	////////////////////////////////////////
	
	
	/**
	 * this method creates an arraylist of wall objects for game creation
	 * 
	 * inputs are civilisations that will apply to each player's walls
	 * 
	 * @param civPlayer0 CivInterface
	 * @param civPlayer1 CivInterface
	 * @param civPlayer2 CivInterface
	 * @param civPlayer3 CivInterface
	 * @param warlord Arraylist of warlords
	 * @return arraylist of walls
	 */
	public static ArrayList<Wall> createGameWalls(CivInterface civPlayer0, CivInterface civPlayer1, CivInterface civPlayer2, CivInterface civPlayer3, ArrayList<Warlord> warlord){
		ArrayList<Wall> toReturn = new ArrayList<Wall>();
		
		for (int i = 0; i < 14; i++){
			toReturn.add(new Wall(wallTopLeftX[i],wallTopLeftY[i],wallTopLeftW[i],wallTopLeftH[i], 0, civPlayer0));
			warlord.get(0).addBrick();
			toReturn.add(new Wall(wallTopRightX[i],wallTopRightY[i],wallTopRightW[i],wallTopRightH[i], 1, civPlayer1));
			warlord.get(1).addBrick();
			toReturn.add(new Wall(wallBottomLeftX[i],wallBottomLeftY[i],wallBottomLeftW[i],wallBottomLeftH[i], 2, civPlayer2));
			warlord.get(2).addBrick();
			toReturn.add(new Wall(wallBottomRightX[i],wallBottomRightY[i],wallBottomRightW[i],wallBottomRightH[i], 3, civPlayer3));
			warlord.get(3).addBrick();
		}
		
		return toReturn;
	}
	/**
	 * this method creates an arraylist of paddle objects for game creation
	 * 
	 * inputs are civilisations that will apply to each player's paddles
	 * @param civPlayer0 CivInterface
	 * @param civPlayer1 CivInterface
	 * @param civPlayer2 CivInterface
	 * @param civPlayer3 CivInterface
	 * @return arraylist of paddles
	 */
	public static ArrayList<Paddle> createGamePaddles(CivInterface civPlayer0, CivInterface civPlayer1, CivInterface civPlayer2, CivInterface civPlayer3){
		ArrayList<Paddle> toReturn = new ArrayList<Paddle>();
		
		//for (int i = 0; i < 4; i++){
			toReturn.add(new Paddle(100,279, civPlayer0.getPaddleWidth(),10, 0, civPlayer0));
			toReturn.add(new Paddle(600,279, civPlayer0.getPaddleWidth(),10, 1, civPlayer1));
			toReturn.add(new Paddle(100,453, civPlayer0.getPaddleWidth(),10, 2, civPlayer2));
			toReturn.add(new Paddle(600,453, civPlayer0.getPaddleWidth(),10, 3, civPlayer3));
		//}
		
		
		return toReturn;
	}
	/**
	 * 
	 * this method creates a single game ball and returns it in arraylist format.
	 * 
	 * this is because the game has support for multiple balls on the game screen at once.
	 * @return arraylist of balls
	 */
	public static ArrayList<Ball> createGameBalls(){
		ArrayList<Ball> toReturn = new ArrayList<Ball>();
		
		
		
		Ball temp = new Ball(768/2,768/2,5,5,20);
		//Ball temp = new Ball(768/2,768/2,xVel,yVel,20);
		toReturn.add(temp);
		
		
		return toReturn;
	}
	/**
	 * this method creates an arraylist of warlord objects for game creation
	 * 
	 * inputs are civilisations that will apply to each player's warlord
	 * @param civPlayer0 CivInterface
	 * @param civPlayer1 CivInterface
	 * @param civPlayer2 CivInterface
	 * @param civPlayer3 CivInterface
	 * @return Arraylist of warlords
	 */
	public static ArrayList<Warlord> createGameWarlords(CivInterface civPlayer0, CivInterface civPlayer1, CivInterface civPlayer2, CivInterface civPlayer3){
		ArrayList<Warlord> toReturn = new ArrayList<Warlord>();
		
		//samurai, viking, amazon, roman
		toReturn.add(new Warlord(0,0,100,100,0, civPlayer0));
		toReturn.add(new Warlord(668,0,100,100,1, civPlayer1));
		toReturn.add(new Warlord(0,668,100,100,2, civPlayer2));
		toReturn.add(new Warlord(668,668,100,100,3, civPlayer3));
		
		
		return toReturn;
	}

}
