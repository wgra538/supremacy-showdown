package supremacyshowdown.game.interfaces;

import javafx.scene.image.Image;

/***
 * Interface for civilizations in the "Supremacy Showdown" game.
 */
public interface CivInterface {
	
    /***
     *  Set the civilization type.
     * @param civ int Civ
     */
    void setCiv(int civ);
	
    /***
     *  Set the paddle width for the civilization.
     * @param paddleWidth int
     */
    void setPaddleWidth(int paddleWidth);
    
    /***
     *  Set the paddle speed for the civilization.
     * @param paddleSpeed int
     */
    void setPaddleSpeed(int paddleSpeed);

    /***
     * @return the type of civilization.
     */
    int getCivilization();
    
    /***
     * @return the width of the paddle.
     */
    int getPaddleWidth();
    
    /***
     * @return the speed of the paddle.
     */
    int getPaddleSpeed();
    
    /***
     * @return the image path for the vertical fill of the paddle.
     */
    Image getPaddleFillV();
    
    /***
     * @return the image path for the horizontal fill of the paddle.
     */
    Image getPaddleFillH();
    
    /***
     * @return the image path for the vertical fill of the brick.
     */
    Image getBrickV();
    
    /***
     * @return the image path for the horizontal fill of the brick.
     */
    Image getBrickH();
	
    /***
     * @return the image path for the default sprite of the civilization.
     */
    Image getSpriteDefault();
    
    /***
     * @return the image path for the default sprite of the civilization.
     */
    Image getSpriteSurprised();
    
    /***
     * @return the image path for the default sprite of the civilization.
     */
    Image getSpritePortrait();

	
}
