package supremacyshowdown.game.models;


import supremacyshowdown.game.interfaces.IBall;

public class Ball implements IBall {
	
	private int xVelocity;
	private int yVelocity;
	private float xPos;
	private float yPos;

	private int[] outerLimitX;
	private int[] outerLimitY;
	
	private int dimension;
	
	/**
	 * constructs a ball with the following parameters:
	 * 
	 * @param initialX - int, starting x position
	 * @param initialY - int, starting y position
	 * @param startVelX - int, starting X velocity
	 * @param startVelY - int, starting y velocity
	 * @param diameter - int, diameter of ball
	 */
	public Ball(int initialX, int initialY, int startVelX, int startVelY, int diameter) {
		
		xPos = initialX;
		yPos = initialY;
		xVelocity = startVelX;
		yVelocity = startVelY;
		
		dimension = diameter;
		
		outerLimitX = new int[8];
		outerLimitY = new int[8];
		
		
		this.setPixels();
		
	}
	/**
	 * fills 2 parallel arrays of ints,
	 * representing X and Y values of the coordinates that the ball takes up
	 */
public void setPixels() {
		
		// sets the main bouncing points for the ball, based on coordinates
		
		for (int i = 0; i<8; i++){
			
			switch(i){
			case 7:	outerLimitX[i] 	= (int)xPos;
					outerLimitY[i] 	= (int)yPos;break;
					
			case 6:	outerLimitX[i] 	= ((int)xPos + dimension) - 1;	
					outerLimitY[i] 	= (int)yPos;break;
					
			case 5:	outerLimitX[i] 	= (int)xPos;	
					outerLimitY[i] 	= ((int)yPos + dimension) - 1;break;
					
			case 4:	outerLimitX[i] 	= ((int)xPos + dimension) - 1;
					outerLimitY[i] 	= ((int)yPos + dimension) - 1;break;
					
			case 3:	outerLimitX[i] 	= (int)((float)xPos + (float)dimension/(float)2);
					outerLimitY[i] 	= (int)yPos;break;
			
			case 2:	outerLimitX[i] 	= (int)((float)xPos + (float)dimension/(float)2);
					outerLimitY[i] 	= (int)yPos+dimension;break;
			
			case 1:	outerLimitX[i] 	= (int)xPos;
					outerLimitY[i] 	= (int)((float)yPos + (float)dimension/(float)2);break;
			
			case 0:	outerLimitX[i] 	= (int)xPos+dimension;
					outerLimitY[i] 	= (int)((float)yPos + (float)dimension/(float)2);break;
			}
			
			
			
			
		}
		
	}
	@Override
	public void setXPos(int x) {
		xPos = x;
		setPixels();

	}

	@Override
	public void setYPos(int y) {
		yPos = y;
		setPixels();
	}

	@Override
	public int getXPos() {
		return (int)xPos;
	}

	@Override
	public int getYPos() {
		return (int)yPos;
	}

	@Override
	public void setXVelocity(int dX) {
		xVelocity = dX;
	}

	@Override
	public void setYVelocity(int dY) {
		yVelocity = dY;
	}

	@Override
	public int getXVelocity() {
		return xVelocity;
	}

	@Override
	public int getYVelocity() {
		return yVelocity;
	}
	/**
	 * gets one of the x coordinates that the ball is taking up
	 * @param i - index of wanted coordinate
	 * @return int - coordinate
	 */
	public int getXCoord(int i){
		return outerLimitX[i];
	}
	/**
	 * gets one of the y coordinates that the ball is taking up
	 * @param i - index of wanted coordinate
	 * @return int - coordinate
	 */
	public int getYCoord(int i){
		return outerLimitY[i];
	}
	
	/**
	 * gets the diameter of the ball
	 * @return int - diameter
	 */
	public int getDimension(){
		return dimension;
	}
	/**
	 * adds a value to the x position of the ball
	 * @param x int x
	 */
	public void addXPos(float x){
		xPos += x;
		setPixels();
	}
	/**
	 * adds a value to the y position of the ball
	 * @param y int y
	 */
	public void addYPos(float y){
		yPos += y;
		setPixels();
	}
}
