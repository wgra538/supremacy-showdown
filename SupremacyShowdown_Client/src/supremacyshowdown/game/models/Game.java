package supremacyshowdown.game.models;


import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import javafx.scene.input.KeyCode;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONException;
import org.json.JSONObject;
import supremacyshowdown.game.interfaces.IGame;



public class Game implements IGame {
	
	private boolean finished;
	private ArrayList<Wall> allWalls;
	private ArrayList<Ball> allBalls;
	private ArrayList<Paddle> allPaddles;
	private ArrayList<Warlord> allWarlords;
	private int timeRemaining;
	private int counter;

	private int outerBounds;
	private boolean[] players;
	private Socket smtpSocket = null;
	private DataOutputStream os = null;
	private ObjectInputStream inputStream = null;



	/**
	 * the constructor for a game object. 
	 * Takes in a list of walls, balls, paddles, and warlords. 
	 * It also takes in the number of ticks wanted in the game, 
	 * the upper bounds of the square game area (only one int required as the game area is square), 
	 * and a boolean array describing which players are human and which are AI
	 * 
	 * 
	 * @param listOfWalls		ArrayList Wall
	 * @param listOfBalls		ArrayList Ball
	 * @param listOfPaddles		ArrayList Paddle
	 * @param listOfWarlords	ArrayList Warlord
	 * @param ticksRemaining	int
	 * @param upperBounds		int
	 * @param playerTypes		boolean[4]
	 */
	public Game(ArrayList<Wall> listOfWalls, ArrayList<Ball> listOfBalls, ArrayList<Paddle> listOfPaddles, ArrayList<Warlord> listOfWarlords, int ticksRemaining, int upperBounds, boolean[] playerTypes) {
		/* Receive ArrayList of all walls, balls, paddles, warlords
		 * Set the game status as not finished.
		 * 
		 * 
		 */

		outerBounds = upperBounds;
		
		players = playerTypes;
		
		allWalls = listOfWalls;
		allBalls = listOfBalls;
		allWarlords = listOfWarlords;
		allPaddles = listOfPaddles;
		
		timeRemaining = ticksRemaining;
		
		counter = 0;
		
		finished = false;



		try {
			smtpSocket = new Socket("127.0.0.1", 20500);
			System.out.println("before input stream");
			inputStream = new ObjectInputStream(smtpSocket.getInputStream());
			System.out.println("after input stream");

		}
		catch (UnknownHostException e) {
			System.err.println("Don't know about host: 127.0.0.1");
		}
		catch (IOException e) {
			System.err.println("Couldn't get I/O for the connection to: hostname");
		}

	}
	/**
	 * Ticks the game once, moving all objects by their required amounts.
	 * Inputs are an ArrayList of KeyCodes, to tell what keys have been pressed.
	 */
	public void tick(ArrayList<KeyCode> keysPressed) throws IOException, ClassNotFoundException {
		
		if(!finished){

			JSONObject input_values = null;

			try {
				String temp = (String) inputStream.readObject();
				input_values = new JSONObject(temp);


				try {
					allBalls.get(0).setXPos(input_values.getInt("bx0"));
					allBalls.get(0).setYPos(input_values.getInt("by0"));
					allPaddles.get(0).setXPos(input_values.getInt("px0"));
					allPaddles.get(0).setYPos(input_values.getInt("py0"));
					allPaddles.get(1).setXPos(input_values.getInt("px1"));
					allPaddles.get(1).setYPos(input_values.getInt("py1"));
					allPaddles.get(2).setXPos(input_values.getInt("px2"));
					allPaddles.get(2).setYPos(input_values.getInt("py2"));
					allPaddles.get(3).setXPos(input_values.getInt("px3"));
					allPaddles.get(3).setYPos(input_values.getInt("py3"));

					for (int i = 0; i< 4; i++){
						// check if we need to flip paddles
						if (to_paddle_orientation(allPaddles.get(i).hOrV()) != input_values.getInt("po"+i)) allPaddles.get(i).flip();

						// check if any warlords have died
						if (input_values.getInt("wl" + allWarlords.get(i).getPlayerIndex())== 0
								&& !allWarlords.get(i).isDead()) {
							allWarlords.get(i).kill();
						}
						// update bricks left info for each warlord
						allWarlords.get(i).setBricksLeft(input_values.getInt("wlb" + allWarlords.get(i).getPlayerIndex()));
					}

					for (int i = 0; i<56; i++) {

						if(input_values.getInt("w_" +
								Integer.toString(allWalls.get(i).getXPos()) + "_" +
								Integer.toString(allWalls.get(i).getYPos()))
								== 0
								&& !allWalls.get(i).isDestroyed()) {
							allWalls.get(i).destroy();
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
			catch (NullPointerException e){
				System.out.println(e);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			timeRemaining -= 1;
		}
		else {
			smtpSocket.close();
		}
		
	}

	public int to_paddle_orientation(Paddle.orientation input){

		if (input == Paddle.orientation.HORIZONTAL){
			return 0;
		}
		return 1;
	}
	/**
	 * Returns a boolean that is true if the game is over
	 */
	@Override
	public boolean isFinished() {
		
		return finished;
	}
	/**
	 * Takes an int input, and sets the number of ticks remaining in the game to that value
	 */
	@Override
	public void setTimeRemaining(int ticks) {
		timeRemaining = ticks;
	}
	/**
	 * returns an int, of the number of ticks left in the game.
	 * @return int ticks
	 */
	public int getTimeRemaining() {
		return timeRemaining;
	}
	/**
	 * checks for a win.
	 * If a warlord has won, set the state of the game to finished
	 * and tell the warlord that it has won.
	 */
	public void checkWin(){
		
		
		if(timeRemaining > 0){							// if there's still time left, check for only 1 warlord alive
			int a = allWarlords.size();
			int b = 0;
			
			for (int i = 0; i<allWarlords.size(); i++){
				if(allWarlords.get(i).isDead()){
					a--;
				}
				else {
					b = i;
				}
			}
			if(a == 1){
				allWarlords.get(b).setWin();
				finished = true;
			}
		}
		
		else {											// if there's no time left, return the warlord with the most bricks
			finished = true;
			int index = 0;
			for (int i = 0; i < allWarlords.size(); i++){
				if (allWarlords.get(i).getBricks() > allWarlords.get(index).getBricks()){
					index = i;
				}
			}
			allWarlords.get(index).setWin();
		}
	}
	/**
	 * returns an arraylist of all the walls in the game.
	 * @return ArrayList Wall
	 */
	public ArrayList<Wall> getWalls(){
		return allWalls;
	}
	/**
	 * returns an arraylist of all the balls in the game.
	 * @return ArrayList Ball
	 */
	public ArrayList<Ball> getBalls(){
		return allBalls;
	}
	/**
	 * returns an arraylist of all the warlords in the game.
	 * @return ArrayList Warlord
	 */
	public ArrayList<Warlord> getWarlords(){
		return allWarlords;
	}
	/**
	 * returns an arraylist of all the paddles in the game.
	 * @return ArrayList Paddle
	 */
	public ArrayList<Paddle> getPaddles(){
		return allPaddles;
	}
}
