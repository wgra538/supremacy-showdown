package supremacyshowdown.game.models;


import javafx.scene.image.Image;
import supremacyshowdown.game.interfaces.CivInterface;
import supremacyshowdown.game.interfaces.IWarlord;

public class Warlord implements IWarlord {
	private int xPos;
	private int yPos;
	private boolean alive;
	
	private int width; // Width is distance from center - kind of like radius on a circle
	private int height; // Height is the distance from center perpendicular to width.
	private int[] outerLimitX;
	private int[] outerLimitY;
	private int noOfPixels;
	private int bricksLeft;
	private int playerIndex;
	
	private Image inGame;
	private Image outNormal;
	private Image outShock;
	
	private boolean shocked;
	
	
	private boolean win;
	/**
	 * The Warlord constructor:
	 * sets initial X and Y values, as well as width and height. 
	 * Also includes a playerindex (position on the board), and a civ to get it's details from.
	 * @param initialX int x
	 * @param initialY int y
	 * @param w int width
	 * @param h int height
	 * @param i int playerIndex
	 * @param civ CivInterface civType
	 */
	public Warlord(int initialX, int initialY, int w, int h, int i, CivInterface civ) {
		
		
		inGame = civ.getSpriteDefault();
		outNormal = civ.getSpritePortrait();
		outShock = civ.getSpriteSurprised();
		
		xPos = initialX;
		yPos = initialY;
		width = w;
		height = h;
		alive = true;
		playerIndex = i;
		shocked = false;
		
		noOfPixels = width*height;
		
		outerLimitX = new int[noOfPixels];
		outerLimitY = new int[noOfPixels];
		
		setPixels();
		win = false;
	}
	/**
	 * sets the pixels that the warlord takes up
	 * used for collision detection
	 */
	public void setPixels(){
		
		int i = 0;
		
		for (int x = xPos; x<xPos + width; x++){
			for (int y = yPos; y<yPos + height; y++){
				
					outerLimitX[i] 	= x;
					outerLimitY[i] 	= y;
					
					i++;
			}
		}
	}

	@Override
	public void setXPos(int x) {
		xPos = x;
		setPixels();
	}

	@Override
	public void setYPos(int y) {
		yPos = y;
		setPixels();
	}
	/**
	 * returns the x position of the warlord
	 * @return int x
	 */
	public int getXPos() {
		return xPos;
	}

	/**
	 * returns the y position of the warlord
	 * @return int y
	 */
	public int getYPos() {
		return yPos;
	}
	/**
	 * Kills the warlord, making it unable to win, and not alive
	 */
	public void kill() {
		alive = false;
		win = false;
	}

	@Override
	public boolean isDead() {
		return !alive;
	}
	/**
	 * makes the warlord think it has won!
	 */
	public void setWin() {
		win = true;
	}

	@Override
	public boolean hasWon() {
		return win;
	}
	/**
	 * gets a list of X coordinates that the warlord takes up.
	 * To be used in conjunction with getYCoord()
	 * @return int[] xcoords
	 */
	public int[] getXCoord(){
		return outerLimitX;
	}
	/**
	 * gets a list of Y coordinates that the warlord takes up.
	 * To be used in conjunction with getXCoord()
	 * @return int[] ycoords
	 */
	public int[] getYCoord(){
		return outerLimitY;
	}
	/**
	 * increases brick count of the warlord
	 */
	public void addBrick(){
		bricksLeft++;
	}
	/**
	 * decreases brick count of the warlord
	 */
	public void removeBrick(){
		bricksLeft--;
	}
	/**
	 * returns the number of bricks associated with the warlord
	 * @return int bricks
	 */
	public int getBricks(){
		return bricksLeft;
	}
	/**
	 * returns the playerIndex of the warlord (e.g. 1 for top left)
	 * @return int playerIndex
	 */

	public void setBricksLeft(int input) {bricksLeft = input;}


	public int getPlayerIndex(){
		return playerIndex;
	}
	/**
	 * returns width of the warlord
	 * @return int width
	 */
	public int getWidth(){
		return width;
	}
	/**
	 * return height of the warlord
	 * @return int height
	 */
	public int getHeight(){
		return height;
	}
	/**
	 * returns the required filepath for the portrait off the game screen
	 * @return String filepath
	 */
	public Image getNormalPic(){
		if(!shocked){			// if the warlord is fine, return it's normal face
			return outNormal;
		}
		else{					// if the warlord is shocked (having just lost a brick), return it's shocked face
			return outShock;
		}
		
	}
	/**
	 * returns filepath to in-game warlord pic
	 * @return String filepath
	 */
	public Image getGamePic(){
		return inGame;
	}
	/**
	 * makes the out of game sprite no longer shocked
	 */
	public void resetSprite() {
		shocked = false;
	}
	/** 
	 * sets the out of game sprite to be shocked
	 */
	public void shock(){
		shocked = true;
	}
}
