package supremacyshowdown.group22.game.model;

import java.util.ArrayList;

import javafx.scene.input.KeyCode;

/***
 * Interface for use in testing high-level game systems in a Warlords-like game.
 */
public interface IGame {

    boolean isFinished();
    
    void setTimeRemaining(int seconds);


	void tick(ArrayList<KeyCode> keysPressed);

}
