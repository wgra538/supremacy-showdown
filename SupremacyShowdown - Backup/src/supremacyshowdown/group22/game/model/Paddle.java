package supremacyshowdown.group22.game.model;


import javafx.scene.image.Image;

public class Paddle implements IPaddle {
	private int xPos;
	private int yPos;
	private int width;
	private int height;
	private int[] outerLimitX;
	private int[] outerLimitY;
	
	
	private int noOfPixels;
	private int paddleOwner;
	
	private Image hImageString;
	private Image vImageString;
	
	private int speed;
	
	private orientation currentOrientation;
	
	public enum orientation {
		VERTICAL, HORIZONTAL
	}
	
	public Paddle(int initialX, int initialY, int initialWidth, int initialHeight, int owner, CivInterface civ) {
		
		xPos = initialX;
		yPos = initialY;
		
		speed = civ.getPaddleSpeed();
		
		height = initialHeight;
		paddleOwner = owner;
		
		width = civ.getPaddleWidth();
		
		vImageString = civ.getPaddleFillH();
		hImageString = civ.getPaddleFillH();
		
		currentOrientation = orientation.HORIZONTAL;
		
		if(width < 0){
			width = 1;
		}
		if(height < 0){
			height = 1;
		}
		// NOTES: width has to be odd in order to make the flipping from vertical to horizontal
		//		  not terrible. If the width is inputted as an even number, I just add one to it
		//		  to make it an odd number
		if(width%2 == 0){
			width++;
		}
		
		noOfPixels = width*height;
		
		outerLimitX = new int[noOfPixels];
		outerLimitY = new int[noOfPixels];
		
		setPixels();
		
	}
	
	public int getSpeed(){
		return speed;
	}
	
	public void flip(){
		
		if (currentOrientation == orientation.HORIZONTAL){
			currentOrientation = orientation.VERTICAL;
			xPos = xPos + (width - 1)/2;
			yPos = yPos - (width - 1)/2;
		}
		else {
			currentOrientation = orientation.HORIZONTAL;
			xPos = xPos - (height - 1)/2;
			yPos = yPos + (height - 1)/2;
		}
		
		int temp = height;
		height = width;
		width = temp;
		
		setPixels();
	}
	/**
	 * sets the pixels that the paddle takes up
	 */
	public void setPixels() {
		
		int i = 0;
		
		
		
		for (int x = xPos; x<xPos + width; x++){					//for every x pixel
			for (int y = yPos; y>yPos - height; y--){				//and every y pixel
				
				outerLimitX[i] 	= x;	
				outerLimitY[i] 	= y;
				
				i++;
				
			}
		}
		
	}

	@Override
	public void setXPos(int x) {
		xPos = x;
		setPixels();
	}

	@Override
	public void setYPos(int y) {
		yPos = y;
		setPixels();
	}
	/**
	 * returns the x position of the paddle
	 * @return int x
	 */
	public int getXPos() {
		return xPos;
	}
	/**
	 * returns the y position of the paddle
	 * @return int y
	 */
	public int getYPos() {
		return yPos;
	}
	/**
	 * returns a list of x coordinates that the paddle takes up
	 * @return int[] xcoords
	 */
	public int[] getXCoord(){
		return outerLimitX;
	}
	/**
	 * returns a list of y coordinates that the paddle takes up
	 * @return int[] ycoords
	 */
	public int[] getYCoord(){
		return outerLimitY;
	}
	/**
	 * returns the playerIndex of the owner of the paddle
	 * @return int playerIndex
	 */
	public int getOwner(){
		return paddleOwner;
	}
	/**
	 * returns whether the paddle is horizontal or vertical
	 * @return orientation
	 */
	public orientation hOrV() {
		return currentOrientation;
	}
/**
 * returns the width of the paddle
 * @return int width
 */
	public int getWidth() {
		
		return width;
	}
	/**
	 * returns the height of the paddle
	 * @return int height
	 */
	public int getHeight() {
		return height;
	}
	/**
	 * returns the filepath to the image that the paddle takes on when it is horizontal
	 * @return String filepath
	 */
	public Image getHImage(){
		return hImageString;
	}
	/**
	 * returns the filepath to the image that the paddle takes on when it is vertical
	 * @return String filepath
	 */
	public Image getVImage(){
		return vImageString;
	}

}
