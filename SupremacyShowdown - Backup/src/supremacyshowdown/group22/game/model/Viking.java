package supremacyshowdown.group22.game.model;

import javafx.scene.image.Image;

/**
	* This object represents the "Viking" civilization.
	* 
	*  @author M GOODALL
	*  @version 08/04/2017
*/

public class Viking implements CivInterface {
	
	private int civ;
	private int paddleWidth;
	private int paddleSpeed;
	private Image paddleFillV;
	private Image paddleFillH;
	private Image brickV;
	private Image brickH;
	private Image spriteDefault;
	private Image spriteSurprised;
	private Image spritePortrait;
	
	/*
	 * Default values in the constructor
	 * 
	 */
	public Viking() {
		civ = 3;
		paddleWidth = 60;	// Moderately wider paddle
		paddleSpeed = 8;	// Moderately slower speed

		paddleFillV = new Image("VikingPaddleV.png");
		paddleFillH = new Image("VikingPaddleH.png");
		brickV = new Image("VikingBrickV.png");
		brickH = new Image("VikingBrickH.png");
		spriteDefault = new Image("VikingDefault.png");
		spriteSurprised = new Image("VikingSurprised.png");
		spritePortrait = new Image("VikingPortrait.png");

	}
	
	/*
	 * Setter methods
	 */
	@Override
	public void setCiv(int c)	{
		civ = c;
	}
	
	@Override
	public void setPaddleWidth(int pw)	{
		paddleWidth = pw;
	}
	
	@Override
	public void setPaddleSpeed(int ps)	{
		paddleSpeed = ps;
	}

	
	/*
	 * Getter methods
	 */
	@Override
	public int getCivilization()	{
		return civ;
	}
	
	@Override
	public int getPaddleWidth()	{
		return paddleWidth;
	}
	
	@Override
	public int getPaddleSpeed()	{
		return paddleSpeed;
	}

	@Override
	public Image getPaddleFillV() {	return paddleFillV;	}

	@Override
	public Image getPaddleFillH() {
		return paddleFillH;
	}

	@Override
	public Image getBrickV() {
		return brickV;
	}

	@Override
	public Image getBrickH() {
		return brickH;
	}

	@Override
	public Image getSpriteDefault() {
		return spriteDefault;
	}

	@Override
	public Image getSpriteSurprised() {
		return spriteSurprised;
	}

	@Override
	public Image getSpritePortrait() {
		return spritePortrait;
	}

}

	