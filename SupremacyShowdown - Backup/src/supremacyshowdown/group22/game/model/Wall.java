package supremacyshowdown.group22.game.model;

import javafx.scene.image.Image;

public class Wall implements IWall {
	
	private int xPos;
	private int yPos;
	private boolean alive;
	
	private int width; // width is number of pixels horizontally, from the top left
	private int height; // Height is number of pixels vertically, from the top left
	private int[] outerLimitX;
	private int[] outerLimitY;
	private int[] angle;
	private int noOfPixels;
	private int wallOwner;
	
	private Image hImagePath;
	private Image vImagePath;

	public Wall(int initialX, int initialY, int w, int h, int owner, CivInterface civ) {
		xPos = initialX;
		yPos = initialY;
		width = w;
		height = h;
		wallOwner = owner;
		alive = true;
		
		// general constructor stuff, setting variables
		vImagePath = civ.getBrickV();
		hImagePath = civ.getBrickH();
		
		
		if(width < 0){
			width = 1;
		}
		if(height < 0){
			height = 1;
		}
		
		noOfPixels = width * height	;
		
		outerLimitX = new int[noOfPixels];
		outerLimitY = new int[noOfPixels];
		angle 		= new int[noOfPixels];
		
		setPixels();
	}
	/**
	 * sets the pixels that a brick takes up, and decides if it's a vertical or horizontal edge
	 */
	public void setPixels(){
		int i = 0;
		
		
		for (int x = xPos; x<xPos + width; x++){					//for every x pixel
			for (int y = yPos; y<yPos + height; y++){				//and every y pixel
																	// set it to either be a vertical or horizontal edge
				if(x == xPos || x == xPos + width - 1){
					angle[i] = 0;
					
				}
				else{
					angle[i] = 1;
					
				}
				outerLimitX[i] 	= x;
					outerLimitY[i] 	= y;
					i++;
				
			}
		}
	}

	@Override
	public void setXPos(int x) {
		xPos = x;
		setPixels();
	}

	@Override
	public void setYPos(int y) {
		yPos = y;
		setPixels();
	}

	@Override
	public boolean isDestroyed() {
		return !alive;
	}
	/**
	 * destroy the wall
	 */
	public void destroy() {
		alive = false;
	}
	/**
	 * get the current x position of the wall
	 * @return int x
	 */
	public int getXPos(){
		return xPos;
	}
	/**
	 * get the current y position of the wall
	 * @return int y
	 */
	public int getYPos(){
		return yPos;
	}
	/**
	 * gets a list of X coordinates that the wall takes up
	 * @return int[] xcoords
	 */
	public int[] getXCoord(){
		return outerLimitX;
	}
	/**
	 * gets a list of Y coordinates that the wall takes up
	 * @return int[] ycoords
	 */
	public int[] getYCoord(){
		return outerLimitY;
	}
	/**
	 * gets a list of angles at the various coordinates 
	 * @return int[] angles
	 */
	public int[] getAngles(){
		return angle;
	}
	/**
	 * returns the playerIndex of the wall's owner
	 * @return int index
	 */
	public int getOwner(){
		return wallOwner;
	}
	/**
	 * returns the brick's width
	 * @return int width
	 */
	public int getWidth(){
		return width;
	}
	/**
	 * returns the brick's height
	 * @return int height
	 */
	
	public int getHeight(){
		return height;
	}
	/**
	 * rereturns the filepath to the brick's horizontal image
	 * @return String filepath
	 */
	public Image getHImagePath(){
		return hImagePath;
	}
	/**
	 * rereturns the filepath to the brick's vertical image
	 * @return String filepath
	 */
	public Image getVImagePath(){
		return vImagePath;
	}

}
