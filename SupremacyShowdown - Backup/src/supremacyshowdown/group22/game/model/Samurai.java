package supremacyshowdown.group22.game.model;

import javafx.scene.image.Image;

/**
	* This object represents the "Samurai" civilization.
	* 
	*  @author M GOODALL
	*  @version 08/04/2017
*/

public class Samurai implements CivInterface {
	
	private int civ;
	private int paddleWidth;
	private int paddleSpeed;
	private Image paddleFillV;
	private Image paddleFillH;
	private Image brickV;
	private Image brickH;
	private Image spriteDefault;
	private Image spriteSurprised;
	private Image spritePortrait;
	
	/*
	 * Default values in the constructor
	 * 
	 */
	public Samurai() {
		civ = 2;
		paddleWidth = 40;	// Narrowest Paddle
		paddleSpeed = 12;	// Fastest Speed

		paddleFillV = new Image("SamuraiPaddleV.png");
		paddleFillH = new Image("SamuraiPaddleH.png");
		brickV = new Image("SamuraiBrickV.png");
		brickH = new Image("SamuraiBrickH.png");
		spriteDefault = new Image("SamuraiDefault.png");
		spriteSurprised = new Image("SamuraiSurprised.png");
		spritePortrait = new Image("SamuraiPortrait.png");

	}
	
	/*
	 * Setter methods
	 */
	@Override
	public void setCiv(int c)	{
		civ = c;
	}
	
	@Override
	public void setPaddleWidth(int pw)	{
		paddleWidth = pw;
	}
	
	@Override
	public void setPaddleSpeed(int ps)	{
		paddleSpeed = ps;
	}

	
	/*
	 * Getter methods
	 */
	@Override
	public int getCivilization()	{
		return civ;
	}
	
	@Override
	public int getPaddleWidth()	{
		return paddleWidth;
	}
	
	@Override
	public int getPaddleSpeed()	{
		return paddleSpeed;
	}

	@Override
	public Image getPaddleFillV() {	return paddleFillV;	}

	@Override
	public Image getPaddleFillH() {
		return paddleFillH;
	}

	@Override
	public Image getBrickV() {
		return brickV;
	}

	@Override
	public Image getBrickH() {
		return brickH;
	}

	@Override
	public Image getSpriteDefault() {
		return spriteDefault;
	}

	@Override
	public Image getSpriteSurprised() {
		return spriteSurprised;
	}

	@Override
	public Image getSpritePortrait() {
		return spritePortrait;
	}

}

	