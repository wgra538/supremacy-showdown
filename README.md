# README

This is a game called Supremacy Showdown. The game is based on the classic 1981 game Warlards, for the Atari 2600. 
There are 3 folders in this Repo:

- "Supremacy showdown" is the base game. Local play only - up to 4 players on the same keyboard. This works great.
- The other 2 folders are the host and client for supporting multiplayer play over a LAN. This doesn't work so great yet, but it does technically work.

### How do I get set up?

The base game (called backup in the repo) is just a java project. I developed using intellij, meaning if you load up the project in that, it should just work out of the box. Hit the play button and have some fun.


For the "LAN" (it only works on one PC) multiplayer, it's a bit more difficult. Start 2 instances of IntelliJ, one with the host version and one with the client version. Start up the host version and select the correct playable classes for your game. Choose AI for all the control types (as network controls aren't supported yet), and get ready to hit start. Start up the client version, select the same classes for each position as you did with the host version. Hit start on the host version, then hit start as fast as you can on the client version. At the moment you should have no issues seeing what is happening in the game from the client version. Some issues are still present.

### Game Controls

Player 1 - top left:
A (move paddle left/down), D (move paddle right/up)

Player 2 - top right:
J (move paddle left/up), L (move paddle right/down)

Player 3 - bottom left:
LEFT ARROW (move paddle left/up), RIGHT ARROW (move paddle right/down)

Player 4 - bottom right:
4 (move paddle left/down), 6 (move paddle right/up)

P - Pause
ESCAPE - (from pause menu) - quit to main menu

PgDn - Skip to end of game (mostly for debug purposes)

### ACKNOWLEDGEMENTS

Some Game Art/Sound Effects sourced online under Creative Commons Zero (CC0).

Civilisation Sprites modified from "DezrasDragons" under CC0 - https://opengameart.org/users/dezrasdragons
Ball sound effect sourced from "artisticDude" under CC0 - https://opengameart.org/content/rpg-sound-pack
Background art modified from "Luis Zuno" under CC0 - https://opengameart.org/users/ansimuz