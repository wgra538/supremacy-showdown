package supremacyshowdown.game.model;

import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

class Server {

    private Thread one;
    private JSONObject values;
    private boolean running;
    private boolean send = false;

    private ServerSocket serverSocket;


    Server(){
        one = new Thread(this::start);
        one.start();
        running = true;
    }
    void set_values(JSONObject input){
        this.values = input;
        send = true;
    }
    private int start(){

        Socket clientSocket;
        DataInputStream input;
        ObjectOutputStream output;

        try {
            serverSocket = new ServerSocket(20500);
            System.out.println("waiting for connection");
            clientSocket = serverSocket.accept();
            System.out.println("Connected");

            input = new DataInputStream(clientSocket.getInputStream());

            output = new ObjectOutputStream(clientSocket.getOutputStream());

            while(running) {
                System.out.println("in running loop");
                if (send) {
                    output.writeObject(this.values.toString());
                    send = false;
                }

            }
            System.out.println("start() finished");
        }
        catch (IOException e) {
            System.out.println(e);
        }

        return 0;
    }

    void stop(){
        System.out.println("stopping server");
        this.running = false;
    }

}


