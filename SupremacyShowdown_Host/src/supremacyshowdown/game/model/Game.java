package supremacyshowdown.game.model;
import java.util.ArrayList;
import javafx.scene.input.KeyCode;
import org.json.JSONObject;


public class Game implements IGame {
	
	private boolean finished;
	private ArrayList<Wall> allWalls;
	private ArrayList<Ball> allBalls;
	private ArrayList<Paddle> allPaddles;
	private ArrayList<Warlord> allWarlords;
	private int timeRemaining;
	private int counter;
	private JSONObject values;
	public Server server_object;

	private int outerBounds;
	private boolean[] players;
	
	
	/**
	 * the constructor for a game object. 
	 * Takes in a list of walls, balls, paddles, and warlords. 
	 * It also takes in the number of ticks wanted in the game, 
	 * the upper bounds of the square game area (only one int required as the game area is square), 
	 * and a boolean array describing which players are human and which are AI
	 * 
	 * 
	 * @param listOfWalls		ArrayList Wall
	 * @param listOfBalls		ArrayList Ball
	 * @param listOfPaddles		ArrayList Paddle
	 * @param listOfWarlords	ArrayList Warlord
	 * @param ticksRemaining	int
	 * @param upperBounds		int
	 * @param playerTypes		boolean[4]
	 */
	public Game(ArrayList<Wall> listOfWalls, ArrayList<Ball> listOfBalls, ArrayList<Paddle> listOfPaddles, ArrayList<Warlord> listOfWarlords, int ticksRemaining, int upperBounds, boolean[] playerTypes) {
		/* Receive ArrayList of all walls, balls, paddles, warlords
		 * Set the game status as not finished.
		 * 
		 * 
		 */

		outerBounds = upperBounds;
		
		players = playerTypes;
		
		allWalls = listOfWalls;
		allBalls = listOfBalls;
		allWarlords = listOfWarlords;
		allPaddles = listOfPaddles;
		
		timeRemaining = ticksRemaining;

		server_object = new Server();

		counter = 0;

		finished = false;

		// start Server here

	}

	/**
	 * Ticks the game once, moving all objects by their required amounts.
	 * Inputs are an ArrayList of KeyCodes, to tell what keys have been pressed.
	 */
	@Override
	public void tick(ArrayList<KeyCode> keysPressed) {

		if(!finished){
			if(counter == 30){
				counter = 0;

				for (int t = 0; t < allWarlords.size(); t++){
					allWarlords.get(t).resetSprite();
				}
				
			}
			else {
				counter ++;
			}
			
			// here paddles are moved based on inputs from the keysPressed arraylist
			
			
			
			for(int u = 0; u < allPaddles.size(); u++ ){
				
				
				
				
				boolean right = false;
				boolean left = false;
				
				/*	if paddle owner = 0: position is top left
				 *	when pressed left:
				 *		if vertical, increase Y until y == 279, then paddle.flip(),
				 *		if horizontal, decrease x value until x == 0;
				 *  
				 *  when pressed right:
				 *  	if vertical, decrease y until y == 0
				 *  	if horizontal, increase x until x == 279, then paddle.flip()
				 */
				if(allPaddles.get(u).getOwner() == 0){
					
					Paddle paddle = allPaddles.get(u);
					
					
					if(players[0]){ 	// if the player is a human
						
						if(keysPressed.contains(KeyCode.A)){				// get their inputs
							left = true;
						}
						if(keysPressed.contains(KeyCode.D)){
							right = true;
						}
					}
					else {				// otherwise get the AI to take over, and decide what inputs to trigger
						int ballX = allBalls.get(0).getXPos();
						int ballY = allBalls.get(0).getYPos();
						
						if(ballX > 279 && paddle.hOrV() == Paddle.orientation.HORIZONTAL){
							right = true;
						}
						else if (paddle.hOrV() == Paddle.orientation.HORIZONTAL){
							if(ballX > paddle.getXPos() + paddle.getWidth()/2){
								right = true;
							}
							else{
								left = true;
							}
						}
						if(ballY > 279 && paddle.hOrV() == Paddle.orientation.VERTICAL){
							left = true;
						}
						else if(paddle.hOrV() == Paddle.orientation.VERTICAL){
							if(ballY > paddle.getYPos() + paddle.getWidth()/2){
								left = true;
							}
							else{
								right = true;
							}
						}
						
					}
						
					if(!(right && left)){ 	// once inputs have been determined, figure out how to move the paddle
											// based on the orientation of the paddle
						
						if(paddle.hOrV() == Paddle.orientation.HORIZONTAL){
						/// horizontal ///
							if(right){
								if(paddle.getXPos() + paddle.getSpeed() > 279){
									paddle.setXPos(279);
									paddle.flip();
								}
								else {
									paddle.setXPos(paddle.getXPos() + paddle.getSpeed());
								}
							}
							if(left){
								if(paddle.getXPos() - paddle.getSpeed() < 0){
									paddle.setXPos(0);
								}
								else {
									paddle.setXPos(paddle.getXPos() - paddle.getSpeed());
								}
							}
						}
						/// vertical ///
						else {
							if(right){
								if(paddle.getYPos() - paddle.getSpeed() < 0){
									paddle.setYPos(0);
								}
								else {
									paddle.setYPos(paddle.getYPos() - paddle.getSpeed());
								}
							}
							if(left){
								if(paddle.getYPos() + paddle.getSpeed() > 279){
									paddle.setYPos(279);
									paddle.flip();
								}
								else {
									paddle.setYPos(paddle.getYPos() + paddle.getSpeed());
								}
							}
						}
					}
				}
				
				
				
				 /*  
				 *  if paddle owner = 1: position is top right
				 *  when pressed left:
				 *  	if vertical, decrease Y until y == 0
				 *  	if horizontal, decrease X until x == 617 - height, then paddle.flip()
				 *  
				 *  when pressed right:
				 *  	if vertical, increase y until y == 279
				 *  	if horizontal, increase x until x == 896-width, then paddle.flip()
				 */
				if(allPaddles.get(u).getOwner() == 1){
					Paddle paddle = allPaddles.get(u);
					if(players[1]){
						if(keysPressed.contains(KeyCode.J)){
							left = true;
						}
						if(keysPressed.contains(KeyCode.L)){
							right = true;
						}
					}
					else {
						
						int ballX = allBalls.get(0).getXPos();
						int ballY = allBalls.get(0).getYPos();
						
						if(ballY > 279 && paddle.hOrV() == Paddle.orientation.VERTICAL){
							right = true;
						}
						else if (paddle.hOrV() == Paddle.orientation.VERTICAL){
							if(ballY > paddle.getYPos() - paddle.getWidth()/2){
								right = true;
							}
							else{
								left = true;
							}
						}
						if(ballX < (768-279) && paddle.hOrV() == Paddle.orientation.HORIZONTAL){
							left = true;
						}
						else if(paddle.hOrV() == Paddle.orientation.HORIZONTAL){
							if(ballX > paddle.getXPos() + paddle.getWidth()/2){
								right = true;
							}
							else{
								left = true;
							}
						}
					}
					if(!(right && left)){
						
						int speed = paddle.getSpeed();
						if(!players[u]){
							speed = (int)((float)speed * (float)0.8);
						}
						if(paddle.hOrV() == Paddle.orientation.HORIZONTAL){
						/// horizontal ///
							if(right){
								if(paddle.getXPos() + speed > 896-paddle.getWidth()){
									paddle.setXPos(896-paddle.getWidth());
								}
								else {
									paddle.setXPos(paddle.getXPos() + speed);
								}
							}
							if(left){
								if(paddle.getXPos() - speed < 488 - paddle.getWidth()/2){
									paddle.setXPos(488 - paddle.getWidth());
									paddle.flip();
								}
								else {
									paddle.setXPos(paddle.getXPos() - speed);
								}
							}
						}
						/// vertical ///
						else {
							if(right){
								if(paddle.getYPos() + speed > 279){
									paddle.setYPos(279);
									paddle.flip();
								}
								else {
									paddle.setYPos(paddle.getYPos() + speed);
								}
							}
							if(left){
								if(paddle.getYPos() - speed < 0){
									paddle.setYPos(0);
								}
								else {
									paddle.setYPos(paddle.getYPos() - speed);
								}
							}
						}
					}
				}
				
				
				/*  if paddle owner = 2: position is bottom left
				 *  when pressed left:
				 *		if vertical, decrease y until y == 489, then paddle.flip()
				 *		if horizontal, decrease x until x == 0
				 *  
				 *  when pressed right:
				 *  	if vertical, increase y until y == 768 - height
				 *  	if horizontal, increase x until x == 279, paddle.flip()
				 */
				if(allPaddles.get(u).getOwner() == 2 ){
					Paddle paddle = allPaddles.get(u);
					if(players[2]){
						if(keysPressed.contains(KeyCode.LEFT)){
							left = true;
						}
						if(keysPressed.contains(KeyCode.RIGHT)){
							right = true;
						}
					}
					
					else {
						
						int ballX = allBalls.get(0).getXPos();
						int ballY = allBalls.get(0).getYPos();
						
						if(ballY < 768-279 && paddle.hOrV() == Paddle.orientation.VERTICAL){
							left = true;
						}
						else if (paddle.hOrV() == Paddle.orientation.VERTICAL){
							if(ballY < paddle.getYPos() - paddle.getWidth()/2){
								left = true;
							}
							else{
								right = true;
							}
						}
						if(ballX > 279 && paddle.hOrV() == Paddle.orientation.HORIZONTAL){
							right = true;
						}
						else if(paddle.hOrV() == Paddle.orientation.HORIZONTAL){
							if(ballX > paddle.getXPos() + paddle.getWidth()/2){
								right = true;
							}
							else{
								left = true;
							}
						}
					}
					if(right ^ left){
						
					int speed = paddle.getSpeed();
					
						speed = (int)((float)speed * (float)0.8);
						if(paddle.hOrV() == Paddle.orientation.HORIZONTAL){
						/// horizontal ///
							if(right){
								if(paddle.getXPos() + speed > 279){
									paddle.setXPos(279);
									paddle.flip();
								}
								else {
									paddle.setXPos(paddle.getXPos() + speed);
								}
							}
							if(left){
								if(paddle.getXPos() - speed < 0){
									paddle.setXPos(0);
								}
								else {
									paddle.setXPos(paddle.getXPos() - speed);
								}
							}
						}
						/// vertical ///
						else {
							if(right){
								if(paddle.getYPos() + speed > 768){
									paddle.setYPos(768);
								}
								else {
									paddle.setYPos(paddle.getYPos() + speed);
								}
							}
							if(left){
								if(paddle.getYPos() - speed < 488 - paddle.getHeight()){
									paddle.setYPos(488 - paddle.getHeight());
									paddle.flip();
								}
								else {
									paddle.setYPos(paddle.getYPos() - speed);
								}
							}
						}
						
					}
				}
				
				/*  if paddle owner = 3: position is bottom left
				 * 	when pressed left:
				 *		if vertical, increase y until y == 768 - height
				 *		if horizontal, decrease x until x == 896 - width, paddle.flip()
				 *  
				 *  when pressed right:
				 *  	if vertical, decrease y until y == 489, paddle.flip
				 *  	if horizontal, increase x until x == 896 - width
				 */
				if(allPaddles.get(u).getOwner() == 3){
					Paddle paddle = allPaddles.get(u);
					if(players[3]){
						if(keysPressed.contains(KeyCode.NUMPAD4)){
							left = true;
						}
						if(keysPressed.contains(KeyCode.NUMPAD6)){
							right = true;
						}
					}
					
					else {
						
						int ballX = allBalls.get(0).getXPos();
						int ballY = allBalls.get(0).getYPos();
						
						if(ballY < 768-279 && paddle.hOrV() == Paddle.orientation.VERTICAL){
							right = true;
						}
						else if (paddle.hOrV() == Paddle.orientation.VERTICAL){
							if(ballY > paddle.getYPos() - paddle.getWidth()/2){
								left = true;
							}
							else{
								right = true;
							}
						}
						if(ballX < 768-279 && paddle.hOrV() == Paddle.orientation.HORIZONTAL){
							left = true;
						}
						else if(paddle.hOrV() == Paddle.orientation.HORIZONTAL){
							if(ballX > paddle.getXPos() + paddle.getWidth()/2){
								right = true;
							}
							else{
								left = true;
							}
						}
					}
					if(!(right && left)){
						
						int speed = paddle.getSpeed();
						if(!players[u]){
							speed = (int)((float)speed * (float)0.8);
						}
						if(paddle.hOrV() == Paddle.orientation.HORIZONTAL){
						/// horizontal ///
							if(right){
								if(paddle.getXPos() + speed > 896 - paddle.getWidth()){
									paddle.setXPos(768 - paddle.getWidth());
								}
								else {
									paddle.setXPos(paddle.getXPos() + speed);
								}
							}
							if(left){
								if(paddle.getXPos() - speed < 488 - paddle.getWidth()){
									paddle.setXPos(488 - paddle.getWidth());
									paddle.flip();
								}
								else {
									paddle.setXPos(paddle.getXPos() - speed);
								}
							}
						}
						/// vertical ///
						else {
							if(right){
								if(paddle.getYPos() - speed < 489 - paddle.getHeight()){
									paddle.setYPos(498 - paddle.getHeight());
									paddle.flip();
								}
								else {
									paddle.setYPos(paddle.getYPos() - speed);
								}
							}
							if(left){
								if(paddle.getYPos() + speed > 768){
									paddle.setYPos(768);
								}
								else {
									paddle.setYPos(paddle.getYPos() + speed);
								}
							}
						}
					}
				}
			}
			
			
			// for each ball in the game
			for (int i = 0; i<allBalls.size(); i++){
				boolean bounced = false;
				// calculate the number of steps required for this tick, based on it's velocities
				int steps = Math.abs(Math.max(allBalls.get(i).getXVelocity(), allBalls.get(i).getYVelocity()));
				// for each of these steps
				for (int g = 0; g<steps; g++){
					bounced = false;
					// move the ball one pixel in the required direction
					allBalls.get(i).addXPos((float)allBalls.get(i).getXVelocity() * (float)((float)1/(float)steps));
					allBalls.get(i).addYPos((float)allBalls.get(i).getYVelocity() * (float)((float)1/(float)steps));
					
					// and check for any wall collisions
					if(!bounced){
						for (int j = 0; j<allWalls.size(); j++){
							if(!allWalls.get(j).isDestroyed()){// this line ignores walls if they've already been destroys
								int[] xcoords = allWalls.get(j).getXCoord();
								int[] ycoords = allWalls.get(j).getYCoord();
								int[] angles = allWalls.get(j).getAngles();
								
								for (int h = 0; h < xcoords.length; h++){
									for (int e = 0; e<4; e++){
										if(		!bounced 
												&& xcoords[h] == allBalls.get(i).getXCoord(e)
												&& ycoords[h] == allBalls.get(i).getYCoord(e)){
											// if the wall and the ball are at the same coordinate, 
											// bounce the ball and destroy the wall, and play the sound

											
											allWalls.get(j).destroy();
											// and reduce the number of bricks the owner has
											int owner = allWalls.get(j).getOwner();
											for (int y = 0; y < allWarlords.size(); y++){
												if(allWarlords.get(y).getPlayerIndex() == owner){
													allWarlords.get(y).removeBrick();
													allWarlords.get(y).shock();
												}
											}
											if(angles[h] == 0){
												allBalls.get(i).setXVelocity(-1*allBalls.get(i).getXVelocity());
											}
											else{
												allBalls.get(i).setYVelocity(-1*allBalls.get(i).getYVelocity());
											}
											
												
											bounced = true;
										}
									}
								}
							}
						}
					}
					//////////////////////////////
					// walls end, paddles start //
					//////////////////////////////
					
					// checking for a collision with a paddle
					
					if(!bounced){
						for (int k = 0; k<allPaddles.size(); k++){
							if(!bounced && !allWarlords.get(allPaddles.get(k).getOwner()).isDead()){
								int[] xcoords = allPaddles.get(k).getXCoord();
								int[] ycoords = allPaddles.get(k).getYCoord();
								
								for (int h = 0; h < xcoords.length; h++){
									if(!bounced){
										for (int e = 0; e<4; e++){
											if(		!bounced 
													&& xcoords[h] == allBalls.get(i).getXCoord(e) 
													&& ycoords[h] == allBalls.get(i).getYCoord(e)){
												
												// check if the paddle is vertical or horizontal, and bounce off appropriately. 
												// This can be improved with more work.
												if(allPaddles.get(k).hOrV() == Paddle.orientation.VERTICAL){
													allBalls.get(i).setXVelocity(-1*allBalls.get(i).getXVelocity());
												}
												else{
													allBalls.get(i).setYVelocity(-1*allBalls.get(i).getYVelocity());
												}
												// and play the collision sound.

												bounced = true;
											}
										}
									}
								}
							}
						}
					}
					
					// if the ball hasn't bounced, see if it collides with any warlords
					if(!bounced){
						for (int k = 0; k<allWarlords.size(); k++){
							if (!allWarlords.get(k).isDead()){
								int[] xcoords = allWarlords.get(k).getXCoord();
								int[] ycoords = allWarlords.get(k).getYCoord();
								
								
								for (int h = 0; h < xcoords.length; h++){
									for (int e = 0; e<4; e++){
										if(		!bounced 
												&& xcoords[h] == allBalls.get(i).getXCoord(e)
												&& ycoords[h] == allBalls.get(i).getYCoord(e)){
											// if it does, play sound and kill the warlord. 
											allWarlords.get(k).kill();
											
											bounced = true;

										}
									}
								}
							}
						}
					}
					// now check if the ball is going to move out of bounds. if it is, flip the appropriate velocity
					if(1 > (allBalls.get(i).getXPos() + ((float)allBalls.get(i).getDimension()/(float)2)) || (outerBounds - 1) < (allBalls.get(i).getXPos() + ((float)allBalls.get(i).getDimension()/(float)2))){
						allBalls.get(i).setXVelocity(-1 * allBalls.get(i).getXVelocity());
					}
					if(1 > (allBalls.get(i).getYPos() + ((float)allBalls.get(i).getDimension()/(float)2)) || (outerBounds - 1) < (allBalls.get(i).getYPos() + ((float)allBalls.get(i).getDimension()/(float)2))){
						allBalls.get(i).setYVelocity(-1 * allBalls.get(i).getYVelocity());
					}
							
					
				}
			}
			// reduce number of remaining ticks by one
			this.setTimeRemaining(this.getTimeRemaining()-1);



            values = new JSONObject();





            values.put("bx0", allBalls.get(0).getXPos());
			values.put("by0", allBalls.get(0).getYPos());
			values.put("px0", allPaddles.get(0).getXPos());
			values.put("py0", allPaddles.get(0).getYPos());
			values.put("px1", allPaddles.get(1).getXPos());
			values.put("py1", allPaddles.get(1).getYPos());
			values.put("px2", allPaddles.get(2).getXPos());
			values.put("py2", allPaddles.get(2).getYPos());
			values.put("px3", allPaddles.get(3).getXPos());
			values.put("py3", allPaddles.get(3).getYPos());

			for (int i = 0; i< 4; i++){
				values.put("po"+i, to_paddle_orientation(allPaddles.get(i).hOrV()));
			}


            for (int i = 0; i<4; i++) {
                if (!allWarlords.get(i).isDead()) {
                    values.put("wl" + Integer.toString(allWarlords.get(i).getPlayerIndex()), 1);
                }
                else {
					values.put("wl" + Integer.toString(allWarlords.get(i).getPlayerIndex()), 0);
                }

                values.put("wlb" + Integer.toString(allWarlords.get(i).getPlayerIndex()), allWarlords.get(i).getBricks());
            }
            for (int i = 0; i<56; i++) {
                if (!allWalls.get(i).isDestroyed()) {
					values.put("w_" + Integer.toString(allWalls.get(i).getXPos()) + "_" + Integer.toString(allWalls.get(i).getYPos()), 1);

                }
                else {
					values.put("w_" + Integer.toString(allWalls.get(i).getXPos()) + "_" + Integer.toString(allWalls.get(i).getYPos()), 0);
                }
            }
            this.server_object.set_values(values);


		}
		// and check to see if anyone won this tick.
		checkWin();
		
	}

	public int to_paddle_orientation(Paddle.orientation input){

		if (input == Paddle.orientation.HORIZONTAL){
			return 0;
		}
		return 1;
	}
	
	/**
	 * Returns a boolean that is true if the game is over
	 */
	@Override
	public boolean isFinished() {
		
		return finished;
	}
	/**
	 * Takes an int input, and sets the number of ticks remaining in the game to that value
	 */
	@Override
	public void setTimeRemaining(int ticks) {
		timeRemaining = ticks;
	}
	/**
	 * returns an int, of the number of ticks left in the game.
	 * @return int ticks
	 */
	public int getTimeRemaining() {
		return timeRemaining;
	}
	/**
	 * checks for a win.
	 * If a warlord has won, set the state of the game to finished
	 * and tell the warlord that it has won.
	 */
	public void checkWin(){
		
		
		if(timeRemaining > 0){							// if there's still time left, check for only 1 warlord alive
			int a = allWarlords.size();
			int b = 0;
			
			for (int i = 0; i<allWarlords.size(); i++){
				if(allWarlords.get(i).isDead()){
					a--;
				}
				else {
					b = i;
				}
			}
			if(a == 1){
				allWarlords.get(b).setWin();
				finished = true;
				this.server_object.stop();
			}
		}
		
		else {											// if there's no time left, return the warlord with the most bricks
			finished = true;
			this.server_object.stop();
			int index = 0;
			for (int i = 0; i < allWarlords.size(); i++){
				if (allWarlords.get(i).getBricks() > allWarlords.get(index).getBricks()){
					index = i;
				}
			}
			allWarlords.get(index).setWin();
		}
	}
	/**
	 * returns an arraylist of all the walls in the game.
	 * @return ArrayList Wall
	 */
	public ArrayList<Wall> getWalls(){
		return allWalls;
	}
	/**
	 * returns an arraylist of all the balls in the game.
	 * @return ArrayList Ball
	 */
	public ArrayList<Ball> getBalls(){
		return allBalls;
	}
	/**
	 * returns an arraylist of all the warlords in the game.
	 * @return ArrayList Warlord
	 */
	public ArrayList<Warlord> getWarlords(){
		return allWarlords;
	}
	/**
	 * returns an arraylist of all the paddles in the game.
	 * @return ArrayList Paddle
	 */
	public ArrayList<Paddle> getPaddles(){
		return allPaddles;
	}
}
